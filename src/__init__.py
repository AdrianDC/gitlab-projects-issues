# Components
from .package.version import Version

# Version
__version__ = Version.get()
