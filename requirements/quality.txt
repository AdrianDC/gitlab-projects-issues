git+https://github.com/AdrianDC/commitizen.git@prerelease # == 3.29.0.AdrianDC.20240813
mypy>=0.971
pre-commit>=3.8.0
pylint>=2.6.0
types-setuptools>=65.3.0
yapf>=0.29.0
